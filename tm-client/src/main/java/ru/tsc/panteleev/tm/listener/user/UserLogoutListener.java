package ru.tsc.panteleev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.user.UserLogoutRequest;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.event.ConsoleEvent;

@Component
public class UserLogoutListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-logout";

    @NotNull
    public static final String DESCRIPTION = "Log out";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userLogoutListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[USER LOGOUT]");
        getAuthEndpoint().logout(new UserLogoutRequest(getToken()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
