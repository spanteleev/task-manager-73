package ru.tsc.panteleev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.task.TaskStartByIdRequest;
import ru.tsc.panteleev.tm.event.ConsoleEvent;
import ru.tsc.panteleev.tm.util.TerminalUtil;

@Component
public class TaskStartByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-start-by-id";

    @NotNull
    public static final String DESCRIPTION = "Start task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskStartByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskEndpoint().startTaskById(new TaskStartByIdRequest(getToken(), id));
    }

}
