package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.panteleev.tm.api.repository.dto.IUserDtoRepository;
import ru.tsc.panteleev.tm.api.service.dto.IUserDtoService;
import ru.tsc.panteleev.tm.dto.model.RoleDto;
import ru.tsc.panteleev.tm.dto.model.UserDto;
import ru.tsc.panteleev.tm.enumerated.RoleType;
import ru.tsc.panteleev.tm.exception.entity.UserNotFoundException;
import ru.tsc.panteleev.tm.exception.field.LoginEmptyException;
import ru.tsc.panteleev.tm.exception.field.PasswordEmptyException;
import ru.tsc.panteleev.tm.exception.field.RoleIncorrectException;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Service
public class UserDtoService implements IUserDtoService {

    @NotNull
    @Autowired
    private IUserDtoRepository repository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void init() {
        initUser("adminWeb", "adminWeb", RoleType.ADMIN);
        initUser("userWeb1", "userWeb1", RoleType.USUAL);
        initUser("userWeb2", "userWeb2", RoleType.USUAL);
        initUser("testWeb", "testWeb", RoleType.USUAL);
    }

    private void initUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (roleType == null) throw new RoleIncorrectException();
        @Nullable final UserDto user = repository.findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    @Override
    @Transactional
    public UserDto createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (roleType == null) throw new RoleIncorrectException();
        @NotNull final UserDto user = new UserDto(login, passwordEncoder.encode(password));
        @NotNull final RoleDto role = new RoleDto(user, roleType);
        user.setRoles(Collections.singletonList(role));
        repository.save(user);
        return user;
    }

    @Override
    @NotNull
    public UserDto findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDto user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

}

