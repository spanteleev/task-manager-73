package unit.repository;

import marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;;
import ru.tsc.panteleev.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.panteleev.tm.configuration.ApplicationConfiguration;
import ru.tsc.panteleev.tm.dto.model.TaskDto;
import ru.tsc.panteleev.tm.util.UserUtil;

import java.util.List;
import java.util.UUID;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class TaskDtoRepositoryTest {

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final TaskDto taskDelete = new TaskDto();

    @NotNull
    private final TaskDto taskSurvivor = new TaskDto();

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("testWeb", "testWeb");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskDelete.setUserId(UserUtil.getUserId());
        taskSurvivor.setUserId(UserUtil.getUserId());
        repository.save(taskDelete);
        repository.save(taskSurvivor);
    }

    @After
    public void clean() {
        repository.deleteAll();
    }

    @Test
    public void findByUserIdAndId() {
        @Nullable final TaskDto taskFind =
                repository.findByUserIdAndId(UserUtil.getUserId(), taskDelete.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(taskFind.getId(), taskDelete.getId());
    }

    @Test
    public void findAllByUserId() {
        List<TaskDto> tasksFind = repository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(tasksFind);
        Assert.assertEquals(2, tasksFind.size());
    }

    @Test
    public void countByUserId() {
        Assert.assertEquals(2, repository.countByUserId(UserUtil.getUserId()));
    }


    @Test
    public void existsByUserIdAndId() {
        Assert.assertFalse(repository.existsByUserIdAndId(UserUtil.getUserId(), UUID.randomUUID().toString()));
        Assert.assertTrue(repository.existsByUserIdAndId(UserUtil.getUserId(), taskDelete.getId()));
    }

    @Test
    public void deleteByUserIdAndId() {
        repository.deleteByUserIdAndId(UserUtil.getUserId(), UUID.randomUUID().toString());
        Assert.assertEquals(2, repository.countByUserId(UserUtil.getUserId()));
        repository.deleteByUserIdAndId(UserUtil.getUserId(), taskDelete.getId());
        Assert.assertFalse(repository.existsByUserIdAndId(UserUtil.getUserId(), taskDelete.getId()));
    }

    @Test
    public void deleteByUserId() {
        repository.deleteByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, repository.countByUserId(UserUtil.getUserId()));
    }

}
