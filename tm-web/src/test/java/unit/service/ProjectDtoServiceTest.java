package unit.service;

import marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.panteleev.tm.api.service.dto.IProjectDtoService;
import ru.tsc.panteleev.tm.configuration.ApplicationConfiguration;
import ru.tsc.panteleev.tm.dto.model.ProjectDto;
import ru.tsc.panteleev.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class ProjectDtoServiceTest {

    @NotNull
    @Autowired
    private IProjectDtoService service;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final ProjectDto projectDelete = new ProjectDto();

    @NotNull
    private final ProjectDto projectSurvivor = new ProjectDto();

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("testWeb", "testWeb");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        service.save(UserUtil.getUserId(), projectDelete);
        service.save(UserUtil.getUserId(), projectSurvivor);
    }

    @After
    public void clean() {
        service.clear(UserUtil.getUserId());
    }

    @Test
    public void create() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(2, service.count(userId));
        service.create(userId);
        Assert.assertEquals(3, service.count(userId));
    }

    @Test
    public void existsById() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertFalse(service.existsById(userId, UUID.randomUUID().toString()));
        Assert.assertTrue(service.existsById(userId, projectDelete.getId()));
    }

    @Test
    public void findById() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertNull(service.findById(userId, UUID.randomUUID().toString()));
        @Nullable final ProjectDto projectFind = service.findById(userId, projectDelete.getId());
        Assert.assertEquals(projectFind.getId(), projectDelete.getId());
    }


    @Test
    public void findAll() {
        List<ProjectDto> projectsFind = service.findAll(UserUtil.getUserId());
        Assert.assertNotNull(projectsFind);
        Assert.assertEquals(2, projectsFind.size());
    }

    @Test
    public void count() {
        Assert.assertEquals(2, service.count(UserUtil.getUserId()));
    }


    @Test
    public void deleteById() {
        @NotNull final String userId = UserUtil.getUserId();
        service.deleteById(userId, projectDelete.getId());
        Assert.assertFalse(service.existsById(userId, projectDelete.getId()));
    }


    @Test
    public void deleteAll() {
        List<ProjectDto> projectsDelete = new ArrayList<>();
        projectsDelete.add(projectDelete);
        service.deleteAll(UserUtil.getUserId(), projectsDelete);
        Assert.assertEquals(1, service.count(UserUtil.getUserId()));
    }

    @Test
    public void clear() {
        service.clear(UserUtil.getUserId());
        Assert.assertEquals(0, service.count(UserUtil.getUserId()));
    }

}
