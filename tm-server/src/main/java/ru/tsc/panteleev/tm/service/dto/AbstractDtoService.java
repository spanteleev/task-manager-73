package ru.tsc.panteleev.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.tsc.panteleev.tm.api.repository.dto.IDtoRepository;
import ru.tsc.panteleev.tm.api.service.dto.IDtoService;
import ru.tsc.panteleev.tm.dto.model.AbstractModelDto;

@Service
public abstract class AbstractDtoService<M extends AbstractModelDto, R extends IDtoRepository<M>> implements IDtoService<M> {

}
