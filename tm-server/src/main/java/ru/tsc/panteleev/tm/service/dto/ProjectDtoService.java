package ru.tsc.panteleev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.panteleev.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.panteleev.tm.api.service.dto.IProjectDtoService;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.panteleev.tm.exception.field.*;
import ru.tsc.panteleev.tm.dto.model.ProjectDto;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDto, IProjectDtoRepository> implements IProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @NotNull
    @Override
    @Transactional
    public ProjectDto create(@Nullable final String userId,
                             @Nullable final String name,
                             @Nullable final String description,
                             @Nullable final Date dateBegin,
                             @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        repository.saveAndFlush(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDto project = findById(userId, id);
        project.setName(name);
        project.setDescription(description);
        repository.saveAndFlush(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto changeStatusById(@Nullable final String userId,
                                       @Nullable String id,
                                       @Nullable Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @NotNull final ProjectDto project = findById(userId, id);
        project.setStatus(status);
        repository.saveAndFlush(project);
        return project;
    }

    @Override
    @Transactional
    public void set(@NotNull Collection<ProjectDto> projects) {
        repository.saveAll(projects);
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@NotNull String userId) {
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public List<ProjectDto> findAll() {
            return repository.findAll();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@NotNull String userId, @Nullable Sort sort) {
        if (sort == null) return findAll(userId);
        return repository.findAllByUserIdSort(userId, sort.getOrderColumn());
    }

    @NotNull
    @Override
    public ProjectDto findById(@NotNull String userId, @NotNull String id) {
        ProjectDto project = repository.findByUserIdAndId(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    @Transactional
    public void removeById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDto project = repository.findByUserIdAndId(userId, id);
        if (project == null) return;
        repository.delete(project);
    }

    @Override
    @Transactional
    public void clear(@NotNull String userId) {
        repository.deleteAll(findAll(userId));
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    public long getSize(@NotNull String userId) {
        return repository.countByUserId(userId);
    }

}
