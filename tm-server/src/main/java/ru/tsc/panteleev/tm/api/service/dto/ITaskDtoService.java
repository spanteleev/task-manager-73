package ru.tsc.panteleev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.dto.model.TaskDto;

import java.util.Date;
import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDto> {

    @NotNull
    List<TaskDto> findAllByProjectId(String userId, String projectId);

    @NotNull
    TaskDto create(@Nullable String userId, @Nullable String name, @Nullable String description,
                   @Nullable Date dateBegin, @Nullable Date dateEnd);

    @NotNull
    TaskDto updateById(@Nullable String userId, @Nullable String id, @Nullable String name,
                       @Nullable String description);

    @NotNull
    TaskDto changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void update(@NotNull TaskDto task);

    void removeTasksByProjectId(@NotNull String userId, @NotNull String projectId);

}
